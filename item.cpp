#include "item.hpp"

#include <cstdint>
#include <deque>
#include <map>
#include <optional>
#include <set>
#include <vector>

#include <glog/logging.h>


namespace crafthelper {


Item::Item(
    std::string name,
    std::optional<Currency> buy,
    std::optional<Currency> sell,
    std::vector<ComponentPair> components,
    std::optional<Count> production
) {
    this->name = std::move(name);
    this->buy = std::move(buy);
    this->sell = std::move(sell);
    this->components = std::move(components);
    this->production = 1;

    if (production) { this->production = *production; }
}

bool Item::isBase() { return this->components.empty(); }

void Item::mapAdd(ComponentCountMap& m, Item* i, const Count& x) {
    auto existing = m.find(i);
    if (existing != m.end()) { m[i] += x; }
    else {
        m.emplace(i, x);
    }
}

Item::ComponentCountMap Item::findComponentsCycleFree(Count multiplier, std::set<std::string>& cycleCheck, bool baseOnly){
    DVLOG(2) << "Searching for base components of: " << this->name;
    ComponentCountMap ret;
    if (!baseOnly || this->isBase()) { ret.emplace(this, multiplier); }

    if (!cycleCheck.insert(this->name).second) {
        LOG(FATAL) << "Circular dependency detected! Attempted to re-insert item: " << this->name;
    }

    for (const auto& component : this->components) {
        // this is slow and essentially impossible to trigger with the current implementation but <shrug>
        auto branchCycleCopy = cycleCheck;
        ComponentCountMap basecomponents = component.first->findComponentsCycleFree(
            multiplier * component.second,
            branchCycleCopy,
            baseOnly
        );
        for (const auto& basecomponent : basecomponents) {
            DVLOG(2) << "Found base component: " << basecomponent.first->name << " x " << basecomponent.second;
            this->mapAdd(ret, basecomponent.first, basecomponent.second);
        }
    }
    return ret;
}

Item::ComponentCountMap Item::findBase(Count multiplier) {
    std::set<std::string> cycleCheck;
    return this->findComponentsCycleFree(multiplier, cycleCheck, true);
}

std::vector<Item::ComponentPair> Item::findComponents(Count multiplier) {
    std::set<std::string> cycleCheck;
    ComponentCountMap components = this->findComponentsCycleFree(multiplier, cycleCheck, false);

    // Let's try to somewhat order these via BFS so they make more sense
    std::vector<Item::ComponentPair> ret;
    std::deque<Item*> search{this};
    std::set<Item*> visited;
    while (!search.empty()) {
        Item* current = search.front();
        search.pop_front();
        if (auto [_, inserted] = visited.insert(current); !inserted) { continue; }
        for (auto [subcomp, _] : current->components) {
            search.push_back(subcomp);
        }
        if (current == this) { continue; }
        ret.emplace_back(current, components[current]);
    }
    return ret;
}


std::optional<Item*> CraftingHelper::find(std::string item) {
    auto it = this->items.find(item);
    if (it == this->items.end()) { return {}; }
    return &(it->second);
}

int CraftingHelper::countItems() { return this->items.size(); }

void CraftingHelper::addItem(const Item& i) { this->items.emplace(i.name, i); }


} // namespace crafthelper