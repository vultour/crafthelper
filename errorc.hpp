#pragma once


namespace crafthelper {
namespace errors {


static constexpr unsigned int HELP_REQUESTED = 1;

static constexpr unsigned int ARG_ERR_DEF_FILE = 10;
static constexpr unsigned int ARG_ERR_COUNT = 11;
static constexpr unsigned int ARG_ERR_BADITEM = 12;
static constexpr unsigned int ARG_ERR_CONFLICT = 13;
static constexpr unsigned int ARG_ERR_UNEXPECTED = 14;

static constexpr unsigned int YAML_ERR_SCHEMA = 20;
static constexpr unsigned int YAML_ERR_SCHEMA_COMPONENTS = 21;


} // namespace errors
} // namespace crafthelper