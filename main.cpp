#include <array>
#include <charconv>
#include <cstring>
#include <iostream>
#include <optional>

#include <getopt.h>

#include <glog/logging.h>
#include <yaml-cpp/yaml.h>

#include "errorc.hpp"
#include "item.hpp"


struct Arguments {
    std::string defFile;
    std::string item;
    double count;
    int base;
    int components;
    int tree;

    unsigned int initialisedDisplayOptions() { return this->base + this->components + this->tree; }
};

const std::string IndentSection = "  ";


void printUsage(char* progName, std::optional<std::string> error) {
    std::cerr <<
        "Usage: "
        << progName
        << " [--help] --definition <yaml-file> [--item <item> [--base | --components | --tree]] [--count <number>]" << std::endl
        << std::endl
        << "A very simple dependency graph explorer, mainly aimed at game item crafting." << std::endl
        << std::endl
        << "Base arguments:" << std::endl
        << "    --definition <yaml-file>" << std::endl
        << "        REQUIRED: Path to a yaml file containing the dependency graph definition." << std::endl 
        << "    --item <item>" << std::endl
        << "        Item to search for. Output can be configured via display arguments." << std::endl
        << "    --count <number>" << std::endl
        << "        Modifies the expected top-level item count." << std::endl
        << "Display arguments:" << std::endl
        << "    --base" << std::endl
        << "        List all base items required to create an item. Base items do not contain any further components." << std::endl
        << "    --components" << std::endl
        << "        List all the individual components and steps required to make an item." << std::endl
        << "    --tree" << std::endl
        << "        Display item's components and their counts in a tree view." << std::endl;
    if (error) {
        std::cerr << "ERROR: " << *error << std::endl;
    }
}


unsigned int countInitialised(const std::vector<std::string>& arr) {
    unsigned int ret = 0;
    for (const auto& str : arr) {
        if (!str.empty()) {
            ++ret;
        }
    }
    return ret;
}


void recursiveTreePrint(crafthelper::Item* item, crafthelper::Item::Count multiplier, int indent) {
    std::string indentString = "";
    for (int i = 0; i < indent; ++i) { indentString += IndentSection; }
    std::cout << indentString << multiplier * item->production << " x " << item->name << std::endl;
    for (const auto& component : item->components) {
        recursiveTreePrint(component.first, component.second * multiplier, indent + 1);
    }
}


int main(int argc, char* argv[]) {
    google::InitGoogleLogging(argv[0]);
    Arguments args{"", "", 1, 0, 0, 0};

    {
        std::array<option, 8> options = {{
            {"help", no_argument, nullptr, 'h'},
            {"definition", required_argument, nullptr, 'd'},
            {"item", required_argument, nullptr, 'i'},
            {"count", required_argument, nullptr, 'x'},
            {"base", no_argument, &args.base, 1},
            {"components", no_argument, &args.components, 1},
            {"tree", no_argument, &args.tree, 1},
            {nullptr, 0, nullptr, 0}
        }};
        int option_index = 0;

        while (true) {
            int c = getopt_long(argc, argv, "hd:i:x:bct", options.data(), &option_index);
            if (c == -1) {
                if (optind != argc) {
                    printUsage(argv[0], std::string("Unexpected argument: ") + argv[optind]);
                    return crafthelper::errors::ARG_ERR_UNEXPECTED;
                }
                break;
            }
            switch (c) {
                case 'h':   
                    printUsage(argv[0], {});
                    return crafthelper::errors::HELP_REQUESTED;
                case 'd':
                    args.defFile = std::string(optarg);
                    break;
                case 'i':
                    args.item = std::string(optarg);
                    break;
                case 'x':
                {
                    uint64_t count = 1; // clang/gcc does not seem to support float conversion (?)
                    if (
                        auto [_, errc] = std::from_chars(
                            optarg,
                            optarg + strlen(optarg),
                            count
                        ); errc != std::errc()
                    ) {
                        LOG(ERROR) << "Failed converting count argument to number: " << optarg;
                        return crafthelper::errors::ARG_ERR_COUNT;
                    }
                    args.count = count;
                    break;
                }
                case '?':
                    LOG(WARNING) << "Something weird happened";
                    break;
                default:
                    LOG(WARNING) << "Something even weirder happened: " << c;
                    break;
            }
        }
    }

    if (args.defFile.empty()) {
        printUsage(argv[0], "No definition file specified");
        return crafthelper::errors::ARG_ERR_DEF_FILE;
    }

    if ((args.initialisedDisplayOptions() < 1) && (!args.item.empty())) {
        printUsage(argv[0], "You need to specify at least one display option");
        return crafthelper::errors::ARG_ERR_CONFLICT;
    }
    if (args.initialisedDisplayOptions() > 1) {
        printUsage(argv[0], "Only one display option can be specified");
        return crafthelper::errors::ARG_ERR_CONFLICT;
    }
    if ((args.initialisedDisplayOptions() > 0) && (args.item.empty())) {
        printUsage(argv[0], "You need to specify an item to search for");
        return crafthelper::errors::ARG_ERR_CONFLICT;
    }

    crafthelper::CraftingHelper ch{};

    YAML::Node def = YAML::LoadFile(args.defFile);
    if (!def.IsMap()) {
        LOG(ERROR) << "Invalid definition schema";
        return crafthelper::errors::YAML_ERR_SCHEMA;
    }
    // Iterate over items
    for (auto itemPair = def.begin(); itemPair != def.end(); ++itemPair) {
        std::string name = itemPair->first.as<std::string>();
        DVLOG(1) << "Found item: " << name;

        std::optional<crafthelper::Currency> buy;
        if (itemPair->second["buy"].IsDefined()){
            buy = itemPair->second["buy"].as<crafthelper::Currency>();
            DVLOG(2) << "Buy: " << *buy;
        }

        std::optional<crafthelper::Currency> sell;
        if (itemPair->second["sell"].IsDefined()) {
            sell = itemPair->second["sell"].as<crafthelper::Currency>();
            DVLOG(2) << "Sell: " << *sell;
        }

        std::optional<crafthelper::Item::Count> production;
        if (itemPair->second["production"].IsDefined()) {
            production = itemPair->second["production"].as<crafthelper::Item::Count>();
            DVLOG(2) << "Production: " << *production;
        }

        std::vector<crafthelper::Item::ComponentPair> components;
        if (itemPair->second["components"].IsDefined()) {
            if (!itemPair->second["components"].IsMap()) {
                LOG(ERROR) << "Invalid component registration for: '" << name << "'. Verify the definition.";
                return crafthelper::errors::YAML_ERR_SCHEMA_COMPONENTS;
            }
            for (auto cmp = itemPair->second["components"].begin(); cmp != itemPair->second["components"].end(); ++cmp) {
                std::optional<crafthelper::Item*> it = ch.find(cmp->first.as<std::string>());
                if (!it) {
                    LOG(ERROR) << "Unknown component for '" << name << "': '" << cmp->first.as<std::string>();
                    continue;
                }
                components.emplace_back(*it, cmp->second.as<crafthelper::Item::Count>());
                //LOG(INFO) << itemPair->second.Type();
                //LOG(INFO) << "We're here! "
                //    << item.IsMap() << " : " << item.Type() << " : " << itemPair->second.Type() << " ; "
                //    << item.as<std::string>() << " = " << itemPair->first.as<std::string>() << std::flush;
            }
        }

        ch.addItem(crafthelper::Item(name, buy, sell, components, production));
    }

    LOG(INFO) << "Created dependency graph. Total items: " << ch.countItems();
    if (args.item.empty()) { return 0; } // Quit early if we're not looking anything up.

    crafthelper::Item* item;
    if (auto tmp = ch.find(args.item); tmp.has_value()) {
        item = *tmp;
    } else {
        LOG(ERROR) << "Could not find the specified item: " << args.item;
        return crafthelper::errors::ARG_ERR_BADITEM;
    }

    if (static_cast<bool>(args.base)) {
        crafthelper::Item::ComponentCountMap base = item->findBase(args.count);
        std::cout << "Base components for " << args.count << " x " << item->name << std::endl;
        for (const auto& baseItem : base) {
            std::cout << baseItem.first->name << ": " << baseItem.second << std::endl;
        }
    }

    if (static_cast<bool>(args.components)) {
        auto components = item->findComponents(args.count);
        std::cout << "Components for " << args.count << " x " << item->name << std::endl;
        for (const auto& component : components) {
            std::cout << "  " << component.first->name << ": " << component.second << std::endl;
        }
    }

    if (static_cast<bool>(args.tree)) {
        recursiveTreePrint(item, args.count, 0);
    }
}
