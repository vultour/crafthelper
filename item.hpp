#pragma once

#include <cstdint>
#include <map>
#include <optional>
#include <set>
#include <vector>

#include <glog/logging.h>


namespace crafthelper {


using Currency = uint64_t;


class Item {
public:
    using Count = double;
    using ComponentPair = std::pair<Item*, Count>;
    using ComponentCountMap = std::map<Item*, Count>;

private:
    void mapAdd(ComponentCountMap& m, Item* i, const Count& x);

    ComponentCountMap findComponentsCycleFree(
        Count multiplier,
        std::set<std::string>& cycleCheck,
        bool baseOnly
    );

public:
    std::string name;
    std::optional<Currency> buy;
    std::optional<Currency> sell;
    std::vector<ComponentPair> components;
    Count production;

    Item(
        std::string name,
        std::optional<Currency> buy,
        std::optional<Currency> sell,
        std::vector<ComponentPair> components,
        std::optional<Count> production
    );

    bool isBase();
    ComponentCountMap findBase(Count multiplier);
    std::vector<ComponentPair> findComponents(Count multiplier);
};


class CraftingHelper {
private:
    std::map<std::string, Item> items;
public:    
    void addItem(const Item& i);
    std::optional<Item*> find(std::string item);
    int countItems();
};


} // namespace crafthelper